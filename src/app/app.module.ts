import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { Reloj02Component } from './reloj02/reloj02.component';


@NgModule({
  declarations: [
    AppComponent,
    Reloj02Component
  ],
  imports: [
    BrowserModule,
   
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
