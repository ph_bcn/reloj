import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Reloj02Component } from './reloj02.component';

describe('Reloj02Component', () => {
  let component: Reloj02Component;
  let fixture: ComponentFixture<Reloj02Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Reloj02Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Reloj02Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
