import { Component, OnInit } from '@angular/core';
import { debug } from 'util';

@Component({
  selector: 'app-reloj02',
  templateUrl: './reloj02.component.html',
    styleUrls: ['./reloj02.component.css']
})
export class Reloj02Component implements OnInit {

  public video: String;
  public YT: any;
  public player: any;
  public reframed: Boolean = false;

  init() {
    var tag = document.createElement('script');
    tag.src = 'https://www.youtube.com/iframe_api';
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
  }

  timeout:number = 1000;
  cambiaTimeout:boolean = true;
  t:void;
  velocidad:number = 0;
  velocidadKM:number;
  aceleracion:number = 1.01;
  relojEncendido:boolean = true;
  datosHora1: string;
  datosHora2: string;
  datosHora3: string;
  datosHora4: string;
  datosHora5: string;
  C: number;
  S: number;
  LC: number;
  SC: number;
  ST: number;
  B: number;
  X: number;
  T: number;
  horas:string = "00";
  minutos:string = "00";
  segundos:string = "00";
  tiempo: number;
  sentido: number;
  estado: number;
  vid:any;

  constructor() { }
  
  ngOnInit() { 

    this.init();
    this.video = 'dP2sg2YM8lM' //video id

    window['onYouTubeIframeAPIReady'] = (e) => {
      this.YT = window['YT'];
      this.reframed = false;
      this.player = new window['YT'].Player('player', {
        videoId: this.video,
        height: '1080',
        width: '1920',  
        playerVars: { 'autoplay': 1, 'controls': 0, 'showinfo':0 , 'autohide':1},
        events: {
          //'onStateChange': this.onPlayerStateChange.bind(this),
          'onError': this.onPlayerError.bind(this),
          'onReady': this.onPlayerReady.bind(this)
            //if (!this.reframed) 
             //this.reframed = true;
             // reframe(e.target.a);*/
            
          
        }
      });
    };



    //this.vid=document.getElementById("player");
 
    this.tiempo = 0;
    this.sentido = 1;
    this.estado = 1;

    this.C = 186279;
    
    this.ST = 1;
    this.S = 0.0; //%respecto a la velocidad de la luz

    this.SC = this.S * this.S ;
    this.LC = this.C * this.C;
    this.B=1-this.SC;
    this.X=Math.sqrt(this.B);
    this.T=this.ST/this.X;
    this.timeout= 1000 / this.T;

    this.velocidad = 1000/this.timeout;
    this.velocidadKM = this.S * 299792.458;
    this.getHora();
  }

  getHora(){

    if (this.cambiaTimeout){
      this.cambiaTimeout = false ;
      setTimeout(() => {

        this.cambiaTimeout = true;
        if (this.aceleracion == 1){
         
        } else {
          if (this.timeout > 1){
            this.timeout/=this.aceleracion;
          } else {
            this.timeout=1;
            this.aceleracion == 1;
          }
          
          this.velocidad=1000/this.timeout;
          this.T=1000/this.timeout;     
          this.X=this.ST/this.T;
          this.B=Math.pow(this.X,2);        
          this.SC = 1 - this.B;
          this.S = Math.sqrt(this.SC);
        }
        
       
        if (this.velocidad <= 2) 
        {
          this.player.setPlaybackRate(Math.round(this.S*10)/10*4);
          console.log(Math.round(this.S*100)/100*4);
        }

       
        this.velocidadKM = this.S * 299792.458;

        let hours: number = Math.floor(this.velocidad / 3600);
        let minutes: number = Math.floor((this.velocidad - (hours*3600)) / 60);
        let seconds: number = this.velocidad - (hours * 3600) - (minutes * 60);

        this.datosHora3 = "1 segundo = " + hours + " horas, " + minutes + " minutos, " + seconds + " segundos";

        this.datosHora2 = "Velocidad Tiempo = 1 x " + this.velocidad;
        this.datosHora4 = "Velocidad km/s = " + this.velocidadKM;
        this.datosHora5 = "% Velocidad LUZ = " + this.S*100 ;  
        this.datosHora1 = "% Aceleración Tiempo = " + (Math.round(this.aceleracion*1000)-1000) ;  
        this.pintaHora();this.getHora();
      }, this.timeout);
    } 
  }
  pintaHora(){
    
    this.tiempo += this.sentido * this.estado;
    let hours: number = Math.floor(this.tiempo / 3600);
    let minutes: number = Math.floor((this.tiempo - (hours*3600)) / 60);
    let seconds: number = this.tiempo - (hours * 3600) - (minutes * 60);

    this.horas = ("0" + hours).slice(-2);
    this.minutos = ("0" + minutes).slice(-2);
    this.segundos = ("0" + seconds).slice(-2);

  }
  
  onPlayerStateChange(event) {
    console.log(event)
    switch (event.data) {
      case window['YT'].PlayerState.PLAYING:
        if (this.cleanTime() == 0) {
          console.log('started ' + this.cleanTime());
        } else {
          console.log('playing ' + this.cleanTime())
        };
        break;
      case window['YT'].PlayerState.PAUSED:
        if (this.player.getDuration() - this.player.getCurrentTime() != 0) {
          console.log('paused' + ' @ ' + this.cleanTime());
        };
        break;
      case window['YT'].PlayerState.ENDED:
        console.log('ended ');
        break;
    };
  };
  //utility
  cleanTime() {
    return Math.round(this.player.getCurrentTime())
  };

  onPlayerReady(event) {
   
    event.target.playVideo();
    this.player.setPlaybackRate(0.1);    

    
  }
  onPlayerError(event) {
    switch (event.data) {
      case 2:
        console.log('' + this.video)
        break;
      case 100:
        break;
      case 101 || 150:
        break;
    };
  };
  
}








